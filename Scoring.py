# -*- coding: utf-8 -*-

'''
ESC Score Board Kivy based Application

author: Christopher Kolbe
email: kontakt@christopherkolbe.de
'''

from kivy.support import install_twisted_reactor

install_twisted_reactor()

from twisted.spread import pb
from twisted.internet import reactor
from twisted.internet import protocol

class MyClientFactory(pb.PBClientFactory, protocol.ReconnectingClientFactory):
  def __init__(self):
    pb.PBClientFactory.__init__(self)
    self.ipaddress = None

  def clientConnectionMade(self, broker):
    Logger.info("Twisted: Started to connect.")
    pb.PBClientFactory.clientConnectionMade(self, broker)

  def buildProtocol(self, addr):
    Logger.info("Twisted: Connected to {}".format(addr))
    return pb.PBClientFactory.buildProtocol(self, addr)

  def clientConnectionLost(self, connector, reason):
    Logger.warning("Twisted: Lost connection.  Reason: {}".format(reason))
    protocol.ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

  def clientConnectionFailed(self, connector, reason):
    Logger.error("Twisted: Connection failed. Reason: {}".format(reason))
    protocol.ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

import pickle

from Participants.Participants import loadParticipants, ParticipantItem

import kivy
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import StringProperty
from kivy.logger import Logger

class ScoreItem(BoxLayout):
  """
  Kivy Item to Display Participant as Input on the Scoring Board.
  """

  country = StringProperty("")

class Scoring(App):
  """
  Kivy App Class this belings to 'Scoring.kv'
  This App expects to get List of Participants, every Participant as dataclass.
  """

  participants = []
  factory = None

  def __init__(self, participants, **kwargs):
    super(Scoring, self).__init__()
    self.participants = participants
    self.factory = MyClientFactory()

    reactor.connectTCP("localhost", 8789, self.factory)

  def build(self):
    '''
    Build Kivy App and start Twister Client.
    '''
    root = self.root
    self.title = 'Scoring'

    Logger.info('Scoring: Participants to load <%s>' % len(self.participants))

    for item in self.participants:

      participant: ParticipantItem = item

      try:
        # load the image
        country = ScoreItem(country=participant.country)
        # add to the main field
        root.add_widget(country)
      except Exception as e:
        Logger.exception('ScoreItem: Unable to load <%s>' % participant.country)

  def callback_update(self):
    '''
    Commando to Sum all Scores and given Points for every Participant.
    Sorted List of Participants get send to Server.

    Remote API 'remote_rebuild' Call is executed.
    '''
    
    root = self.root
    for widget in root.walk(restrict=True):
      if type(widget) is ScoreItem:
        for item in self.participants:

          participant: ParticipantItem = item

          try:
            if participant.country == widget.ids['country'].text:
              if int(widget.ids['score'].text) > 0:
                Logger.info('ScoreItem: <%s>, Score old: <%d>, Score add: <%d>' % (participant.country, participant.score, int(widget.ids['score'].text)))
              participant.score += int(widget.ids['score'].text)
          except Exception as e:
            Logger.exception('ScoreItem: <%s>' % participant.country)
          
          try:
            if participant.country == widget.ids['country'].text:
              if widget.ids['public_voting_done'].active != participant.public_voting_done:
                Logger.info('ScoreItem: <%s>, Score old: <%d>, Score add: <%d>' % (participant.country, participant.score, int(widget.ids['score'].text)))
                participant.public_voting_done = widget.ids['public_voting_done'].active
          except Exception as e:
            Logger.exception('ScoreItem: <%s>' % participant.country)

    # Sort by Score or Country
    self.participants.sort(reverse=True)

    # Set all TextInputs to "0"
    for widget in root.walk(restrict=True):
      if type(widget) is TextInput:
        widget.delete_selection()
        widget.text = "0"

    # Send Participants to Remote and Update the List
    participants_serialized = pickle.dumps(self.participants)
    d = self.factory.getRootObject()
    d.addCallback(lambda object: object.callRemote("rebuild", participants_serialized))
    d.addCallback(lambda echo: Logger.info("Twisted: server echoed: %s" % echo))
    d.addErrback(lambda reason: Logger.error("Twisted: {}".format(str(reason.value))))

  def callback_clear(self):
    '''
    Commando to set all Scoring Inputs to 0 and Hide Participants on the ScoreBoard.

    Remote API 'remote_clear' Call is executed.
    '''

    root = self.root

    # Set all TextInputs to "0"
    for widget in root.walk(restrict=True):
      if type(widget) is TextInput:
        widget.delete_selection()
        widget.text = "0"
    
    d = self.factory.getRootObject()
    d.addCallback(lambda object: object.callRemote("clear"))
    d.addCallback(lambda echo: Logger.info("Twisted: server echoed: %s" % echo))
    d.addErrback(lambda reason: Logger.error("Twisted: {}".format(str(reason.value))))

  def callback_save(self):
    '''
    Commando to save current Results to File './participants.pkl' for later Load Operation.
    '''
    
    with open('participants.pkl', 'wb') as pickle_file:
      pickle.dump(self.participants, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)
      Logger.info("Pickle: Saved Participants to file.")

  def callback_load(self):
    '''
    Commando to load current Results to File './participants.pkl' for later Load Operation.
    Update the Public Voting Information in Scoring UI.
    '''

    with open('participants.pkl', 'rb') as pickle_load:
      self.participants = pickle.load(pickle_load)
      Logger.info("Pickle: Load Participants from file.")
      
      root = self.root
      for widget in root.walk(restrict=True):
        if type(widget) is ScoreItem:
          for item in self.participants:

            participant: ParticipantItem = item
            
            try:
              if participant.country == widget.ids['country'].text:
                widget.ids['public_voting_done'].active = participant.public_voting_done
            except Exception as e:
              Logger.exception('ScoreItem: <%s>' % participant.country)

# the __name__ idiom executes when run from command line but not from import.
if __name__ == '__main__':
  # Load Participants only one Time and Sort by Start Position
  participants = loadParticipants()
  
  Scoring(participants).run()
