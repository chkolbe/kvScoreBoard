# -*- coding: utf-8 -*-

from glob import glob
from os.path import join
from os.path import dirname
from os.path import basename

from dataclasses import dataclass, astuple

import unittest

from kivy.logger import Logger

@dataclass(init=False, repr=False, eq=False, order=False)
class ParticipantItem:
    """Class for keeping track of an Participant."""
    flag_file_path: str
    country: str
    score: int = 0
    progress: float = 0.0
    start: int = 0
    public_voting_done: bool = False

    def __init__(self, flag: str, country: str, start: int):
      self.flag_file_path = flag
      self.country = country
      self.start = start

      self.score = 0
      self.progress = 0.0
      self.public_voting_done = False

    def __repr__(self) -> str:
      return f"Country: {self.country} with Flag: {self.flag_file_path} got Start Position {self.start} with Score of {self.score} and Public Voting is done: {self.public_voting_done}"

    def __eq__(self, other: object) -> bool:
      '''
      We implement Equal based on Participant Country Name.
      Only one Country in the List of Participants is allowed!
      '''
      if not isinstance(other, ParticipantItem):
        return NotImplemented
      return self.country.__eq__(other.country)

    def __ne__(self, other: object) -> bool:
      '''
      We implement Not Equal based on Participant Country Name.
      Only one Country in the List of Participants is allowed!
      '''
      if not isinstance(other, ParticipantItem):
        return NotImplemented
      return self.country.__ne__(other.country)

    def __lt__(self, other: object):
      '''
      We implement Lower Then based on Participant Total Score.
      '''
      if not isinstance(other, ParticipantItem):
        return NotImplemented
      return ((self.score) < (other.score))
  
    def __gt__(self, other: object):
      '''
      We implement Greater Then based on Participant Total Score.
      '''
      if not isinstance(other, ParticipantItem):
        return NotImplemented
      return ((self.score) > (other.score))
  
    def __le__(self, other: object):
      '''
      We implement Lower or Equal based on Participant Total Score.
      '''
      if not isinstance(other, ParticipantItem):
        return NotImplemented
      return ((self.score) <= (other.score))
  
    def __ge__(self, other: object):
      '''
      We implement Greater or Equal based on Participant Total Score.
      '''
      if not isinstance(other, ParticipantItem):
        return NotImplemented
      return ((self.score) >= (other.score))

    def __iter__(self):
      return iter(astuple(self)) 

def loadParticipants(search_pattern='[0-9]*.png'):
  '''
  Returns List of Dictionaries, one Dataclass for every Participant.
  '''
  participants = []

  for file in glob(join('img', 'flags', search_pattern)):
    try:
      
      participant = ParticipantItem(
        file,
        file.replace('.png', '').split('_')[1],
        int(basename(file).split('_')[0])
      )
      
      participants.append(participant)

    except Exception as e:
      Logger.exception('Participants: Unable to load Participant <%s>' % file)

  participants.sort(key=lambda item: item.start)
  Logger.info('Participants: Participants found <%s>' % len(participants))
  
  return participants

class TestCase(unittest.TestCase):
  def test_representation(self):
    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position
    
    self.assertEqual(
      repr(a), 
      "Country: Ukraine with Flag: 01_Ukraine.png got Start Position 1 with Score of 0 and Public Voting is done: False", 
      "String representation does not match!")

  def test_equal(self):
    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    self.assertEqual(a == b, True, "Both Dataclasses are identical.")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "02_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    self.assertEqual(a == b, True, "Flag is different!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      2) # Start_Position

    self.assertEqual(a == b, True, "Start Position is different!")
  
  def test_not_equal(self):
    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Germany", # Country_Name
      1) # Start_Position

    self.assertEqual(a != b, True, "Country name is different!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "02_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    self.assertEqual(a != b, False, "Flag is different!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      2) # Start_Position

    self.assertEqual(a != b, False, "Start Position is different!")
    
  def test_lower_then(self):
    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 0
    b.score = 10

    self.assertEqual(a < b, True, "B is greater then A!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    self.assertEqual(a < b, False, "A and B are equal!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 10
    b.score = 0

    self.assertEqual(a < b, False, "A is greater then B!")

  def test_greater_then(self):
    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 10
    b.score = 0

    self.assertEqual(a > b, True, "A is greater then B!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    self.assertEqual(a > b, False, "A and B are equal!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 10
    b.score = 0

    self.assertEqual(b > a, False, "A is greater then B!")

  def test_lower_or_equal_then(self):
    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 0
    b.score = 10

    self.assertEqual(a <= b, True, "B is greater then A!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    self.assertEqual(a <= b, True, "A and B are equal!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 10
    b.score = 0

    self.assertEqual(a <= b, False, "A is greater then B!")

  def test_greater_or_equal_then(self):
    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 10
    b.score = 0

    self.assertEqual(a >= b, True, "A is greater then B!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    self.assertEqual(a >= b, True, "A and B are equal!")

    a = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    b = ParticipantItem(
      "01_Ukraine.png", # Flag_File
      "Ukraine", # Country_Name
      1) # Start_Position

    a.score = 10
    b.score = 0

    self.assertEqual(b >= a, False, "A is greater then B!")

  def test_iterator(self):
    list_to_test = list()
    list_to_test.append(
      ParticipantItem(
        "01_Ukraine.png", # Flag_File
        "Ukraine", # Country_Name
        1)
    )
    list_to_test.append(
      ParticipantItem(
        "02_Deutschland.png", # Flag_File
        "Deutschland", # Country_Name
        2)
    )
    list_to_test.append(
      ParticipantItem(
        "03_Italien.png", # Flag_File
        "Italien", # Country_Name
        3)
    )

    iterator = iter(list_to_test)

    self.assertEqual(
      next(iterator),
      ParticipantItem(
        "01_Ukraine.png", # Flag_File
        "Ukraine", # Country_Name
        1)
      )

    self.assertEqual(
      next(iterator),
      ParticipantItem(
        "02_Deutschland.png", # Flag_File
        "Deutschland", # Country_Name
        2)
      )
    
    self.assertEqual(
      next(iterator),
      ParticipantItem(
        "03_Italien.png", # Flag_File
        "Italien", # Country_Name
        3)
      )