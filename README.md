# Requirements

1. Check Python 3.8.X is installed with 'python3 --version'
2. Check Virtualenv is installed with 'virtualenv --version'
3. Update virtualenv.ini with Python Path to use
4. Create Virtual Environment `virtualenv venv`
5. (Linux, MacOS) Activate VirtualEnv with `source venv/bin/activate`
5. (Windows) Activate VirtualEnv with `PS venv\bin\activate.ps1`
6. Install Kivy `pip3 install kivy twisted`

# Run the Unittests

1. (Linux, MacOS) Activate VirtualEnv with `source venv/bin/activate`
1. (Windows) Activate VirtualEnv with `PS venv\bin\activate.ps1`
2. Run dataclass Unittests `python3 -m unittest Participants/Participants.py`

# Update the Participants and Background Image
1. Remove all starting Numbers 00..99 from Files in Folder
/img/flags/<00>_Country.png to /img/flags/_Country.png
2. Add Start Position before the _ in the File Name.
/img/flags/_Country.png to /img/flags/<Start Position>_Country.png
3. Import the New Background Image with Resolution of at least 1090 as bg.png under /img/bg.png

# Run the Application

1. (Linux, MacOS) Activate VirtualEnv with `source venv/bin/activate`
1. (Windows) Activate VirtualEnv with `PS venv\bin\activate.ps1`
2. Start the Server (Dashboard) App `python3 ScoreBoard.py`
3. Start the Client (Scoring) App `python3 Scoring.py`

![Start List for the year 2024](preview.png)

![Winner List for the year 2024](result.png)
