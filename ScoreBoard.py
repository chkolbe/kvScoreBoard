# -*- coding: utf-8 -*-

'''
ESC Score Board Kivy based Application

author: Christopher Kolbe
email: kontakt@christopherkolbe.de
'''

from kivy.support import install_twisted_reactor

install_twisted_reactor()

from twisted.spread import pb
from twisted.internet import reactor

import pickle

from Participants.Participants import loadParticipants, ParticipantItem

import kivy
kivy.require('2.1.0')
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.stacklayout import StackLayout
from kivy.properties import StringProperty
from kivy.logger import Logger

class CountryItem(StackLayout):
  """
  Kivy Item to Display Participant as Country on the ScoreBoard.
  """

  flag = StringProperty("")
  country = StringProperty("")
  score = StringProperty("0")

class CountryItemPublicVotingDone(StackLayout):
  """
  Kivy Item to Display Participant as Country on the ScoreBoard.
  """

  flag = StringProperty("")
  country = StringProperty("")
  score = StringProperty("0")

class ScoreBoard(App, pb.Root):
  """
  Kivy App Class this belings to 'ScoreBoard.kv'
  This App expects to get List of Participants, every Participant as dataclass.
  """

  participants = []

  def __init__(self, participants, **kwargs):
    super(ScoreBoard, self).__init__()
    self.participants = participants

  def build(self):
    '''
    Build Kivy App and start Twister Server.
    '''
    root = self.root
    self.title = 'ScoreBoard'
    reactor.listenTCP(8789, pb.PBServerFactory(self))

  def remote_clear(self):
    '''
    Remote API to Hide the Participants from the ScoreBoard.
    '''
    root = self.root

    Logger.info("ScoreBoard: Clear")
    root.clear_widgets()

    return "Done"
  
  def remote_rebuild(self, st):
    '''
    Remote API Display sorted List of Participants on the ScoreBoard.
    '''
    root = self.root
    participants = pickle.loads(st)

    Logger.info('Twisted: Participants to load <%s>' % len(participants))

    root.clear_widgets()

    for item in participants:

      participant: ParticipantItem = item

      if participant.public_voting_done == True:
        try:
          # load the image
          country = CountryItemPublicVotingDone(flag=participant.flag_file_path, 
                                                country=participant.country, 
                                                score=str(participant.score))
          # add to the main field
          root.add_widget(country)
        except Exception as e:
          Logger.exception('Country: Unable to load <%s>' % participant.country)
      
      else:
        try:
          # load the image
          country = CountryItem(flag=participant.flag_file_path, 
                                country=participant.country, 
                                score=str(participant.score))
          # add to the main field
          root.add_widget(country)
        except Exception as e:
          Logger.exception('Country: Unable to load <%s>' % participant.country)

    return "Done"

# the __name__ idiom executes when run from command line but not from import.
if __name__ == '__main__':
  # Load Participants only one Time and Sort by Start Number
  participants = loadParticipants()

  ScoreBoard(participants).run()
